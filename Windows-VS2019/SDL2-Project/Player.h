#ifndef PLAYER_H_
#define PLAYER_H_

#include "SDL2Common.h"
#include "Sprite.h"

// Forward declerations
// improve compile time. 
class Vector2f;
class Animation;

class Player 
{
private:
    
    // Texture which stores the sprite sheet (this
    // will be optimised).
    SDL_Texture*     texture;

    // Player properties
    SDL_Rect targetRectangle;
    float speed;
    Vector2f* position;
    Vector2f* velocity;

    // Sprite information
    static const int SPRITE_HEIGHT = 64;
    static const int SPRITE_WIDTH = 32;
    
    // Animation state
    int state;

    // Animations
    static const int MAX_ANIMATIONS = 5;
    Animation* animations[MAX_ANIMATIONS];

public:
    Player();
    ~Player();

    // Player Animation states
    enum PlayerState{LEFT, RIGHT, UP, DOWN, IDLE};

    void init(SDL_Renderer *renderer);

    void draw(SDL_Renderer *renderer);
    void processInput(const Uint8 *keyStates);
    void update(float timeDeltaInSeconds);

};

#endif